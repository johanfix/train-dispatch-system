package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import edu.ntnu.stud.commands.Command;
import edu.ntnu.stud.commands.Exit;
import edu.ntnu.stud.commands.Print;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * The 'CommandTest' class test the functionality of the 'Command'
 * class for running the application works.These tests were generated using chat-GPT as a tool to ensure
 *  * proper coverage and validation of the 'TrainDeparture' class methods.
 */

public class CommandTest {

  private Register register;

  /**
   * sets up the necessary resources for each test cause.
   */
  @BeforeEach
  void setUp() {
    register = new Register();
  }

  /**
   * Tests the 'run' method for a command that allows the application to continue.
   * Used the 'print' command as example, as it doesn't involve interaction with user.
   */
  @Test
    void testRunContinue() {
    Command printCommand = new Print();
    boolean result = printCommand.run(register);

    assertFalse(result);
  }

  /**
   * Test the 'run' method for the command that exits the application.
   */
  @Test
    void testRunExits() {
    Command exitCommand = new Exit();
    boolean result = exitCommand.run(register);

    assertTrue(result);
  }
}

