package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.Duration;
import java.time.LocalTime;
import org.junit.jupiter.api.*;

/**
 * The 'TrainDepartureTest' class contains JUnit tests for the functionality and exception handling
 * of the 'TrainDeparture' class. These tests were generated using chat-GPT as a tool to ensure
 * proper coverage and validation of the 'TrainDeparture' class methods.
 */
public class TrainDepartureTest {
  TrainDeparture test = new TrainDeparture(12, 0, "R2", "222", "Oslo");

  /**
   * Nested class for the get methods of the 'TrainDeparture' class.
   */
  @Nested
  class GetTest {
    @Test
    void testThatGetDepartureTimeReturnCorrect() {
      assertEquals(LocalTime.of(12, 0), test.getDepartureTime());
      assertEquals("R2", test.getLine());
      assertEquals("222", test.getTrainNumber());
      assertEquals("Oslo", test.getDestination());
    }
  }

  /**
   * Nested test class for the set methods of the 'TrainDeparture' class.
   */

  @Nested
  class SetTest {
    TrainDeparture test = new TrainDeparture(12, 0, "R2", "222", "Oslo");
    int newTrack = 3;
    Duration newDelay = Duration.ofHours(0).plusMinutes(5);

    @Test
    void testThatSetDepartureTimeReturnCorrect() {
      test.setTrack(newTrack);
      test.setDelay(newDelay);
      assertEquals(3, test.getTrack());
      assertEquals(Duration.ofHours(0).plusMinutes(5), test.getDelay());

    }
  }

  /**
   * Nested test class for the exception handling of the 'TrainDeparture' class.
   */

  @Nested
  class ExceptionsTest {
    TrainDeparture test = new TrainDeparture(12, 0, "L1", "14", "Trondheim");

    @Test
    void testSetMethodsWithNegativeValueShouldThrowExceptions() {
      try {
        Assertions.assertThrows(IllegalArgumentException.class, () -> test.setTrack(-10));
      } catch (IllegalArgumentException e) {
        Assertions.fail("The test failed because " + e.getMessage());
      }
    }

    @Test
    void testInvalidHourShouldThrowException() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(-1, 0, "L2", "15", "Bergen"));
    }

    @Test
    void testInvalidMinutesShouldThrowException() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(12, 62, "L3", "16", "Stavanger"));
    }

    @Test
    void testNullLineShouldThrowException() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(12, 60, null, "16", "Stavanger"));
    }

    @Test
    void testNullTrainNumberShouldThrowException() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(12, 60, "L5", null, "Stavanger"));
    }

    @Test
    void testNullDestinationShouldThrowException() {
      assertThrows(IllegalArgumentException.class, () ->
          new TrainDeparture(12, 60, "L5", "17", null));
    }
  }
}

