package edu.ntnu.stud;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalTime;
import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * The 'RegisterTest' class tests the 'Register' class's methods.
 * The developer used chat-GPT as a tool to generate proper JUnit tests for
 * the 'Register' class
 */

public class RegisterTest {

  private Register testRegister;
  private TrainDeparture trainDepartureTest1;
  private TrainDeparture trainDepartureTest2;
  private TrainDeparture trainDepartureTest3;

  /**
   * sets up the necessary resources for each test cause.
   */
  @BeforeEach
  void setUp() {

    testRegister = new Register();

    trainDepartureTest1 = new TrainDeparture(10,  10, "L1", "1", "Paris");
    trainDepartureTest2 = new TrainDeparture(11,  11, "L2", "2", "London");
    trainDepartureTest3 = new TrainDeparture(12,  12, "L3", "3", "Berlin");
  }

  /**
  * Nested test class for testing the 'addTrainDeparture' method.
  */
  @Nested
    class AddTrainDepartureTest {

    /**
     * Tests that adding a train departure is successful.
     */
    @Test
      void testAddTrainDepartureAdds() {
      Register test = new Register();
      test.addTrainDeparture(trainDepartureTest1);

      assertTrue(test.getTrainDepartures().contains(trainDepartureTest1));

    }

    /**
     * Tests that adding a train departure with an existing train number throws an exception.
     */

    @Test
      void testAddTrainDepartureThrowsTrainNumberExist() {
      testRegister.addTrainDeparture(trainDepartureTest2);

      TrainDeparture duplicateTrainDepartureTest2 = new TrainDeparture(11, 11, "L2", "2", "London");

      assertThrows(IllegalArgumentException.class,
          () -> testRegister.addTrainDeparture(duplicateTrainDepartureTest2));
    }

    /**
     * Test that adding a train departure with a departure time in the past throws an exception.
     */

    @Test
      void testAddTrainDepartureThrowsDepartureTimePassed() {
      LocalTime testClock = LocalTime.of(23, 30);

      testRegister.setClock(testClock);

      assertThrows(IllegalArgumentException.class,
            () -> testRegister.addTrainDeparture(trainDepartureTest3));
    }
  }

  /**
   * Nested test class for testing the 'searchByDestination' method.
   */

  @Nested
    class TestSearchTrainNumber {

    /**
     * test searching for a train departure by train number is returning correctly.
     */
    @Test
      void testSearchTrainNumberReturnsTrainDeparture() {
      testRegister.addTrainDeparture(trainDepartureTest1);
      String searchedTrainNumber = "1";
      TrainDeparture result = testRegister.searchTrainNumber(searchedTrainNumber);

      assertNotNull(result);
      assertEquals(searchedTrainNumber, result.getTrainNumber());
    }

    /**
     * test the 'searchTrainNumber' returns null if train departure is not found.
     */

    @Test
      void testSearchTrainNumberReturnsNull() {
      String searchedTrainNumber = "NonExistentTrain";
      TrainDeparture result = testRegister.searchTrainNumber(searchedTrainNumber);

      assertNull(result);
    }
  }

  /**
   * Nested test class for testing the 'searchByDestination' method.
   */

  @Nested
  class TestSearchByDestination {

    /**
     * test searching for train departure by destination returns correct train departure.
     */

    @Test
    void testSearchByDestinationReturnsTrainDeparture() {
      testRegister.addTrainDeparture(trainDepartureTest1);
      String searchedDestination = "Paris";
      TrainDeparture result = testRegister.searchByDestination(searchedDestination);

      assertEquals(searchedDestination, result.getDestination());
    }

    /**
     * test that searching for a non-existing train return null.
     */
    @Test
    void testSearchDestinationReturnsNull() {
      String searchedDestination = "NonExistingTrain";
      TrainDeparture result = testRegister.searchByDestination(searchedDestination);

      assertNull(result);
    }
  }

  /**
   * Test the 'deleteOutDatedTrainDeparture' deletes train departures from the register.
   */

  @Test
    void testDeleteOutDatedTrainDeparturesDeletesOutdated() {

    testRegister.addTrainDeparture(trainDepartureTest1);
    testRegister.addTrainDeparture(trainDepartureTest3);

    testRegister.setClock(LocalTime.of(11, 0));
    testRegister.deleteOutDatedTrainDepartures();

    //Expect that it is only one object in testRegister,
    // after outdatedTrain is removed but existingTrain stays
    assertEquals(1, testRegister.getTrainDepartures().size());
  }

  /**
  * Test the 'sortTrainDeparture' method sorts the train departure register in order of
  * departure time correctly.
  */

  @Test
    void testSortTrainDepartures() {
    testRegister.addTrainDeparture(trainDepartureTest2);
    testRegister.addTrainDeparture(trainDepartureTest3);
    testRegister.addTrainDeparture(trainDepartureTest1);
    testRegister.sortTrainDepartures();

    List<TrainDeparture> sortedDepartures = testRegister.getTrainDepartures();

    assertEquals(LocalTime.of(10, 10), sortedDepartures.get(0).getDepartureTime());
    assertEquals(LocalTime.of(11, 11), sortedDepartures.get(1).getDepartureTime());
    assertEquals(LocalTime.of(12, 12), sortedDepartures.get(2).getDepartureTime());

  }

  /**
   * Test the 'setClock' sets time correctly.
   */

  @Test
    void testSetClockReturnCorrect() {
    LocalTime testTime = LocalTime.of(20, 30);
    testRegister.setClock(testTime);
    assertEquals(LocalTime.of(20, 30), testRegister.getClock());
  }
}
