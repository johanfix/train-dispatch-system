package edu.ntnu.stud;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * The 'Register' class serves as a register for train departures.
 * The class contain a collection of train departures and allows users to manage
 * and perform operations on the collection.
 */

public class Register {

  /**
   * ArrayList of Train departures.
   * The list compiles train departures for the entire system
   */
  private final ArrayList<TrainDeparture> trainDepartures = new ArrayList<>();

  /**
   * Access method for the list of train departures.
   *
   * @return ArrayList of train departures
   */
  public ArrayList<TrainDeparture> getTrainDepartures() {
    return trainDepartures;
  }

  /**
   * Searches for a TrainDeparture using train number as reference.
   *
   * @param searchedTrainNumber The train number to search for
   * @return the train departure object if found, otherwise returns null
   */

  public TrainDeparture searchTrainNumber(String searchedTrainNumber) {
    for (TrainDeparture trainDeparture : getTrainDepartures()) {
      if (trainDeparture.getTrainNumber().equals(searchedTrainNumber)) {
        return trainDeparture;
      }
    }
    return null;
  }

  /**
   * Adds a new TrainDeaprture to the register.
   *
   * @param newTrainDeparture The trainDeparture to add to the register.
   * @throws IllegalArgumentException if train number already exist
   * @throws IllegalArgumentException if the train departure already has passed
   */
  public void addTrainDeparture(TrainDeparture newTrainDeparture) {
    String newTrainNumber = newTrainDeparture.getTrainNumber();

    if (searchTrainNumber(newTrainNumber) != null) {
      throw new IllegalArgumentException("Train number already exist");
    }
    if (newTrainDeparture.getDepartureTimeWithDelay().isBefore(getClock())) {
      throw new IllegalArgumentException("The given departure time har already passed");
    }

    trainDepartures.add(newTrainDeparture);
  }

  /**
   * Searches for train departure using destination as reference.
   *
   * @param searchedDestination the destination searched for
   * @return TrainDeparture object if found, otherwise null
   */
  public TrainDeparture searchByDestination(String searchedDestination) {
    for (TrainDeparture trainDeparture : getTrainDepartures()) {
      if (trainDeparture.getDestination().equalsIgnoreCase(searchedDestination)) {
        return trainDeparture;
      }
    }
    return null;
  }

  /**
   * Sort the train departures based on their departure time.
   */

  public void sortTrainDepartures() {
    Comparator<TrainDeparture> sortedOrder = Comparator
        .comparing(TrainDeparture::getDepartureTime);

    getTrainDepartures().sort(sortedOrder);
  }

  /**
   * Deletes train departures that have already passed.
   */
  public void deleteOutDatedTrainDepartures() {

    getTrainDepartures().removeIf(
        trainDeparture -> trainDeparture.getDepartureTimeWithDelay().isBefore(getClock()));
  }

  /**
   * Edits the track of a train based on its train number.
   *
   * @param trainNumber The train number of the train to edit
   * @param track The new track the number to set.
   */
  public void editTrack(String trainNumber, int track) {
    searchTrainNumber(trainNumber).setTrack(track);
  }

  /**
   * Represent the current time used by the register.
   */
  private LocalTime clock = LocalTime.MIN;

  /**
   * Gets the current time.
   *
   * @return the current time
   */
  public LocalTime getClock() {
    return clock;
  }

  /**
   * Sets the current time.
   *
   * @param clock the new time to set
   */
  public void setClock(LocalTime clock) {
    this.clock = clock;
  }
}
