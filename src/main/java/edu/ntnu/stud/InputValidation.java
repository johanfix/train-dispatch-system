package edu.ntnu.stud;

import java.util.Scanner;

/**
 * Utility class for input validation for the Train Dispatch application.
 * Provides methods for obtaining validated integer and string inputs.
 */

public class InputValidation {

  /**
   * Scanner object for reading input from the console.
   */
  public static Scanner scanner = new Scanner(System.in);

  /**
   * Reads and validates an integer input from the user.
   *
   * @return the method is recursive and return itself
   * @throws NumberFormatException if the input is not a valid integer
   */
  public static int intProvider() {
    try {
      return Integer.parseInt(scanner.nextLine());
    } catch (NumberFormatException e) {
      System.out.println("Invalid input. Please enter a whole number");
      return intProvider();
    }
  }

  /**
   * Reads and validates a String from the user.
   *
   * @return stringProvider method, the method is recursive and returns itself
   */
  public static String stringProvider() {
    try {
      return scanner.nextLine();
    } catch (Exception e) {
      System.out.println("Invalid text. Please try again");
      return stringProvider();
    }
  }

  /**
   * Reads and validates hours input from the user.
   *
   * @return The validated hours input (between 0 and 23).
   * @throws NumberFormatException If the input is not a valid integer.
   */
  public static int hoursProvider() {
    int hours;
    while (true) {
      try {
        System.out.println("Enter hours");
        hours = Integer.parseInt(scanner.nextLine());
        if (hours >= 0 && hours < 24) {
          return hours;

        } else {
          System.out.println("Invalid hour input. Please enter a value between 0 and 23.");
        }
      } catch (NumberFormatException e) {
        System.out.println("Invalid input. Please enter a valid whole number.");

      }
    }
  }

  /**
   * Reads and validates minutes input from the user.
   *
   * @return The validated minutes input (between 0 and 59)
   * @throws NumberFormatException If the input is not a valid integer.
   */

  public static int minutesProvider() {
    int minutes;
    while (true) {
      try {
        System.out.println("Enter minutes");
        minutes = Integer.parseInt(scanner.nextLine());
        if (minutes >= 0 && minutes < 60) {
          return minutes;
        } else {
          System.out.println("Invalid minutes input. Please enter a value between 0 and 59.");
        }
      } catch (NumberFormatException e) {
        System.out.println("Invalid input. Please enter a valid whole number.");
      }
    }
  }
}