package edu.ntnu.stud;

import edu.ntnu.stud.commands.Add;
import edu.ntnu.stud.commands.Command;
import edu.ntnu.stud.commands.Exit;
import edu.ntnu.stud.commands.Print;
import edu.ntnu.stud.commands.SearchDestination;
import edu.ntnu.stud.commands.SearchTrainN;
import edu.ntnu.stud.commands.SetDelay;
import edu.ntnu.stud.commands.SetTrack;
import edu.ntnu.stud.commands.UpdateClock;
import java.time.Duration;

/**
 * This class serves as the user interface for interaction with the train departure system.
 * It provides a set of commands for users to manage train departures.
 */

public class UserInterface {

  Register mainRegister = new Register();

  /**
   * A list of all system commands, each corresponding to a user's action choice.
   */
  private static final Command[] COMMANDS = {
      new Print(),
      new Add(),
      new SetTrack(),
      new SetDelay(),
      new SearchTrainN(),
      new SearchDestination(),
      new UpdateClock(),
      new Exit()
  };

  /**
   * Handles user input by presenting a set of options and executing the selected command.
   *
   * @param register The register to be modified based on user commands.
   */
  public static void start(Register register) {
    init(register);

    boolean exitRequested = false;

    while (!exitRequested) {
      String choice = "Choose one of the following options:\n";
      System.out.println(choice);
      for (int i = 0; i < COMMANDS.length; i++) {
        System.out.println((i + 1) + " " + COMMANDS[i].getName());
      }

      int input = InputValidation.intProvider();

      if (input < 1 || input > COMMANDS.length) {
        System.out.println("Please try again");
        continue;
      }

      exitRequested = COMMANDS[input - 1].run(register);
    }
  }

  /**
   * Initilizes the user interface by configuring an initial set of train departures.
   *
   * @param register The register to be initialized.
   */
  public static void init(Register register) {
    try {
      TrainDeparture train1 = new TrainDeparture(12, 0, "F2", "14", "Hammerfest");
      TrainDeparture train2 = new TrainDeparture(12, 30, "F3", "12", "Nøtterøy");
      TrainDeparture train3 = new TrainDeparture(13, 10, "L1", "67", "Brussel");
      TrainDeparture train4 = new TrainDeparture(15, 0, "L4", "17", "Kristiansand");
      TrainDeparture train5 = new TrainDeparture(21, 14, "L1", "10", "Hamar");
      TrainDeparture train6 = new TrainDeparture(8, 10, "F7", "13", "Molde");

      register.addTrainDeparture(train1);
      register.addTrainDeparture(train2);
      register.addTrainDeparture(train3);
      register.addTrainDeparture(train4);
      register.addTrainDeparture(train5);
      register.addTrainDeparture(train6);

      train1.setTrack(2);
      train1.setDelay(Duration.ofMinutes(20));
      train2.setTrack(12);
      train2.setDelay(Duration.ofMinutes(12));
      train4.setTrack(1);
      train5.setTrack(20);
      train5.setDelay(Duration.ofMinutes(30));
      train6.setTrack(1);
      train6.setDelay(Duration.ofHours(2));


    } catch (IllegalArgumentException e) {
      System.out.println("Invalid train departure ");
    }
  }
}
