package edu.ntnu.stud;

/**
 * This is the main class for the train dispatch application.
 */
public class TrainDispatchApp {

  /**
   * The main entry point for the application.
   *
   * @param args Command-line arguments
   */
  public static void main(String[] args) {
    UserInterface ui = new UserInterface();
    ui.start(ui.mainRegister);

  }
}

