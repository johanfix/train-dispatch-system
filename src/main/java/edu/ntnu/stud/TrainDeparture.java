package edu.ntnu.stud;


import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * The 'TrainDeparture' class represents train departures and provides methods
 * to manage and retrieve information about them.
 */
public class TrainDeparture {
  private final LocalTime departureTime;
  private int hour;
  private int minutes;
  private final String line;
  private final String trainNumber;
  private final String destination;
  private int track;
  private Duration delay;

  /**
   * Constructs a TrainDeparture object with the provided details.
   *
   * @param hour The hour value (0-23) for departure time.
   * @param minutes The minutes value (0-59) for departure time.
   * @param line The line or route of the train.
   * @param trainNumber The number associated with the train.
   * @param destination The destination of the train.
   *
   * @throws IllegalArgumentException if any of the input values are invalid.
   *
   * @implNote "int Track" and "LocalTime delay" are initialized to -1 and 00:00 respectively.
   */
  public TrainDeparture(int hour,
                         int minutes,
                         String line,
                         String trainNumber,
                         String destination) throws IllegalArgumentException {


    if (hour < 0 || hour > 23) {
      throw new IllegalArgumentException("Invalid hour");
    }
    if (minutes < 0 || minutes > 60) {
      throw new IllegalArgumentException("Invalid minutes");
    }
    if (line == null) {
      throw new IllegalArgumentException("A line input is required");
    }
    if (line.isBlank()) {
      throw new IllegalArgumentException("Line cant be empty");
    }
    if (trainNumber == null) {
      throw new IllegalArgumentException("A train number is required");
    }
    if (destination == null) {
      throw new IllegalArgumentException("A destination is required");
    }

    departureTime = LocalTime.of(hour, minutes);
    this.line = line;
    this.trainNumber = trainNumber;
    this.destination = destination;
    this.track = -1;
    this.delay = Duration.ofHours(0).plusMinutes(0);
  }


  /**
   * Get method for departure time.
   *
   * @return The departure time of the train departure
   */
  public LocalTime getDepartureTime() {
    return departureTime;
  }

  /**
   * Get method for the line.
   *
   * @return The train departures line
   */

  public String getLine() {
    return line;
  }
  /**
   * Get method for the train number.
   *
   * @return The train number of the train departure
   */

  public String getTrainNumber() {
    return trainNumber;
  }
  /**
   * Get method for destination.
   *
   * @return The destination of the train departure
   */

  public String getDestination() {
    return destination;
  }

  /**
   * Get method for the track.
   *
   * @return The track number. If not set, return -1
   */

  public int getTrack() {
    return track;
  }

  /**
   * Get method for the delay.
   *
   * @return The delay duration. If not set, returns Duration of zero.
   */
  public Duration getDelay() {
    return delay;
  }

  /**
   * Returns the departure time with the applied delay.
   *
   * @return The departure time with delay
   */

  public LocalTime getDepartureTimeWithDelay() {
    return getDepartureTime().plus(getDelay());
  }
  /**
   * Sets the track for a train departure.
   *
   * @param track The track number to be set. Must not be less than -1.
   * @throws IllegalArgumentException if the track is less than -1.
   */

  public void setTrack(int track) {
    if (track < -1) {
      throw new IllegalArgumentException("Track cant be less than -1");
    }
    this.track = track;
  }

  /**
   * Sets the delay duration of the train departure.
   *
   * @param delay The delay to be set. Must not be negative.
   * @throws IllegalArgumentException if the delay is negative
   *
   */

  public void setDelay(Duration delay) {
    if (delay.isNegative()) {
      throw new IllegalArgumentException("Delay cant be negative");
    }
    this.delay = delay;
  }

  @Override
  public String toString() {

    StringBuilder output = new StringBuilder();

    output.append(String.format("%-12s", getDepartureTime()));
    output.append(String.format("%-12s", getLine()));
    output.append(String.format("%-12s", getTrainNumber()));
    output.append(String.format("%-16s", getDestination()));

    if (getDelay().isZero()) {
      output.append(String.format("%-16s", " "));
    } else {
      output.append(String.format("%-16s", getDepartureTimeWithDelay()));
    }
    if (getTrack() == -1) {
      output.append(String.format("%-16s", " "));

    } else {
      output.append(String.format("%-16d", getTrack()));
    }
    return output.toString();
  }
}
