package edu.ntnu.stud.commands;

import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;

/**
 * The 'SetTrack' class represents a command for setting a track for a train departure.
 */
public class SetTrack extends Command {
  public SetTrack() {
    super("Set track", "Set track on a train departure");
  }

  /**
   * Executes the 'SetTrack' method allowing user to set the track of given
   * train departure.
   *
   * @param register the Register contains all the train departures
   * @return Always return false as it does not end the application
   */
  @Override
  public boolean run(Register register) {

    System.out.println("Type in train number");
    String chosenTrain = InputValidation.stringProvider();

    if (register.searchTrainNumber(chosenTrain) != null) {
      System.out.println("Type in track number ");
      int chosenTrack = InputValidation.intProvider();

      register.editTrack(chosenTrain, chosenTrack);

    } else {
      System.out.println("Train departure with train number " + chosenTrain + " was not found.");
    }


    return false;
  }
}
