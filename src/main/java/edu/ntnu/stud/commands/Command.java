package edu.ntnu.stud.commands;

import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDispatchApp;

/**
 * The 'Command' class serves as a superclass for the applications commands.
 * Subclasses must implement the 'run' method to define the behavior of the command.
 */
public abstract class Command {

  /**
   * The name of a command.
   */
  String name;
  /**
   * A brief description of the commands functionality.
   */
  String description;

  /**
   * Construct a command with a specific name and description.
   *
   * @param name The name of the command
   * @param description Description of the command
   */

  public Command(String name, String description) {
    this.name = name;
    this.description = description;
  }

  /**
   * retrieves the commando name.
   *
   * @return the name of the command
   */

  public String getName() {
    return name;
  }

  /**
   * retrieves a brief commando description.
   *
   * @return a brief description of the command
   */

  public String getDescription() {
    return description;
  }
  /**
  * Execute the command, providing access to the applications register.
  *
  * @return true if the loop should exit after a command is executed,
  *
  * return false if the loop should continue.
  */

  public abstract boolean run(Register register);
}

