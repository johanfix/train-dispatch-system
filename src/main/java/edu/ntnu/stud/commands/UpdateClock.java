package edu.ntnu.stud.commands;

import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;
import java.time.LocalTime;

/**
 * The 'UpdateClock' class represent a command for updating the systems clock.
 */
public class UpdateClock extends Command {

  public UpdateClock() {
    super("Clock update", "Updates the system clock");
  }

  /**
   * Execute the 'UpdateClock' method by allowing user to set the clock.
   *
   * @param register the register of the application
   * @return Always returns false as it does not end the application
   */
  @Override
  public boolean run(Register register) {

    int hour = InputValidation.hoursProvider();
    int minutes = InputValidation.minutesProvider();

    register.setClock(LocalTime.of(hour, minutes));
    return false;
  }
}
