package edu.ntnu.stud.commands;

import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.Command;

/**
 * The 'Exit' class represent a command to exit the application.
 * This command terminal the program when executed.
 *
 */
public class Exit extends Command {
  public Exit() {
    super("Exit", "Exit the program");
  }

  /**
  * Execute the 'Exit' command.
  *
  * @param register the Register, but does not use it in this command
  * @return True, terminates the application.
  */

  @Override
  public boolean run(Register register) {
    return true;
  }
}
