package edu.ntnu.stud.commands;

import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDeparture;
import java.time.Duration;


/**
 * The 'SetDelay' class represent a command for setting the delay of a train departure.
 * The user can specify whether the delay is in hours and minutes or only minutes.
 */

public class SetDelay extends Command {
  public SetDelay() {
    super("Set delay", "Set the delay of a train departure");
  }

  /**
   * Executes the 'SetDelay' method by interacting with user to
   * find train departure by train number as reference and
   * attempting to set delay.
   *
   * @param register the register of train departures
   * @return false to indicate that the application should continue
   */
  @Override
  public boolean run(Register register) {

    System.out.println("Type in train number");
    String chosenTrain = InputValidation.stringProvider();

    TrainDeparture trainDeparture = register.searchTrainNumber(chosenTrain);

    if (trainDeparture != null) {

      System.out.println("If the train is delayed for more than an hour: press [1]"
          + "\n If the train is delayed only for minutes: press [2]");
      switch (InputValidation.intProvider()) {
        case 1:
          int hours = InputValidation.hoursProvider();
          int minutes = InputValidation.minutesProvider();
          trainDeparture.setDelay(Duration.ofHours(hours).plusMinutes(minutes));
          break;
        case 2:
          int minutes2 = InputValidation.minutesProvider();
          trainDeparture.setDelay(Duration.ofHours(0).plusMinutes(minutes2));
          break;
        default:
          System.out.println("Invalid choice. Please try again.");
          break;
      }
    } else {
      System.out.println("Train with train number " + "'" + chosenTrain + "'" + " was not found.");
    }
    return false;
  }
}
