package edu.ntnu.stud.commands;

import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDeparture;
import edu.ntnu.stud.TrainDispatchApp;
import edu.ntnu.stud.commands.Command;

/**
 * The 'SearchDestination' class represent a command for searching
 * for a train departures using destination.
 *
 */
public class SearchDestination extends Command {
  public SearchDestination() {
    super("Search train by destination", "Search for train departure based on the destination");
  }

  /**
   * Executing the 'SearchDestination' method,
   * searching for train departures and displaying them if
   * the search is successful.
   *
   * @param register the register for the application
   * @return false to indicate the application should continue
   */

  @Override
  public boolean run(Register register) {

    System.out.println("Type in destination");
    String chosenDestination = InputValidation.stringProvider();

    TrainDeparture foundDeparture = register.searchByDestination(chosenDestination);

    if (foundDeparture != null) {
      System.out.println(foundDeparture.toString());
    } else {
      System.out.println("Train departure with destination '"
          + chosenDestination + "' was not found.");
    }

    return false;
  }
}
