package edu.ntnu.stud.commands;

import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDeparture;


/**
 * The 'Add' class represents a command to add a new train departure to the register.
 * It interacts with user through the 'InputValidation' class to get valid information
 * such as train number, line, departure time and destination. After information is given,
 * the method attempts to create a new 'TrainDeparture' object and then add it
 * to the Register.
 * If the addition is successful, the new train departure is registered. Otherwise, an
 * error message is displayed to the user. This class extends the `Command` class and
 * implements the `run` method to execute the add operation
 *
 * @see TrainDeparture Represents a train departure object.
 * @see InputValidation Handels input validation for user interactions.
 * @see Register Manages the collections of train departures.
 */
public class Add extends Command {
  public Add() {
    super("Add new train departure", " Add new train departure to the register");
  }

  /**
   * Executes the add operation by interacting with the user to gather train information
   * and attempting to add the new train departure to the register.
   *
   * @param register The register contains all the existing train departures.
   * @return Always return false as it does not end the application.
   */
  @Override
  public boolean run(Register register) {

    System.out.println("Type in train number");
    String newTrainNumber = InputValidation.stringProvider();

    System.out.println("Type in departure time");
    int hours = InputValidation.hoursProvider();
    int minutes = InputValidation.minutesProvider();


    System.out.println("Type in line");
    String line = InputValidation.stringProvider();

    System.out.println("Type in destination");
    String destination = InputValidation.stringProvider();

    try {
      TrainDeparture newTrainDeparture = new TrainDeparture(hours, minutes, line, newTrainNumber,
          destination);
      register.addTrainDeparture(newTrainDeparture);

    } catch (IllegalArgumentException e) {
      System.out.println(e.getMessage());
    }
    return false;
  }
}
