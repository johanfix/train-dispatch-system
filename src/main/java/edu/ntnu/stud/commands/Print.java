package edu.ntnu.stud.commands;

import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDeparture;

/**
 * The 'Print' class represent a command to print the attributes of a train departure
 * in a formatted information board.
 * This command sort and filters train departures before printing the information board.
 */
public class Print extends Command {
  public Print() {
    super("Print information board", "Print the information board");
  }

  /**
   * Executing the 'Print' command, first sort and delete outdated train departures,
   * then printing the departures formatted as an information board.
   *
   * @param register the register containing train departures
   * @return false to indicate that the application should continue
   */
  @Override
  public boolean run(Register register) {

    register.sortTrainDepartures();
    register.deleteOutDatedTrainDepartures();

    System.out.println(register.getClock()
        + " --------------------------- Train register --------------------------- \n"
        + String.format("%-12s", "Time")
        + String.format("%-12s", "Line")
        + String.format("%-12s", "Train nr")
        + String.format("%-16s", "Destination")
        + String.format("%-16s", "New time")
        + String.format("%-16s", "Track"));

    for (TrainDeparture trainDeparture : register.getTrainDepartures()) {
      System.out.println(trainDeparture.toString());
    }
    return false;
  }
}

