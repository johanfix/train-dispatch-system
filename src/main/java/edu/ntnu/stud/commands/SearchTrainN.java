package edu.ntnu.stud.commands;



import edu.ntnu.stud.InputValidation;
import edu.ntnu.stud.Register;
import edu.ntnu.stud.TrainDeparture;
import edu.ntnu.stud.TrainDispatchApp;
import java.util.Scanner;

/**
 * The 'SearchTrainN' class represent a command for searching
 * for a train departure using the train number as reference.
 */
public class SearchTrainN extends Command {
  public SearchTrainN() {
    super("Search train by number", "Search train departure based on the train number");
  }

  /**
   * Executing the 'SearchTrainN' method, searching for a train departure
   * with train number as reference and displaying it if the search was successfully.
   *
   * @param register the register of the application
   * @return false to indicate the application should continue
   */

  @Override
  public boolean run(Register register) {

    System.out.println("Type in train number");
    String chosenTrainNumber = InputValidation.stringProvider();

    TrainDeparture foundDeparture = register.searchTrainNumber(chosenTrainNumber);

    if (foundDeparture != null) {
      System.out.println(foundDeparture.toString());
    } else {
      System.out.println("Train departure with train number "
          + chosenTrainNumber + " was not found.");
    }

    return false;
  }
}
