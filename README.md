# Portfolio project IDATT1003 - 2023
This file uses Mark Down syntax. For more information see [here](https://www.markdownguide.org/basic-syntax/).

STUDENT NAME = Johanne Fixdal  
STUDENT ID = 111684

## Project description

A Java-based train dispatch application which manage train departures for a train station within a day.
The application provides user different options to manage the train departures. 
The key functionalities is;
 - Adding new train departures
 - Displaying all the train departures
 - Setting the delay of a departure
 - Searching for departures by destination
 - Searching for departures by train number
 - Updating the systems clock


## Project structure

The project's source code is in the [scr](src) directory, which is divided in 
two main subdirectories, source code and tests.

[main/java/edu.ntnu.stud](src/main/java/edu): Contains the Java applications source files.
- [App.java](src/main/java/edu/ntnu/stud/TrainDispatchApp.java): Responsible for launching the application.
- [Register.java](src/main/java/edu/ntnu/stud/Register.java): Contains the register of all train departures and manages the operations related to the register.
- [TrainDeparture.java](src/main/java/edu/ntnu/stud/TrainDeparture.java): Represent the entity-class for train departures.
- [UserInterface.java](src/main/java/edu/ntnu/stud/UserInterface.java): Handles user interaction for the application
- [InputValidation.java](src/main/java/edu/ntnu/stud/InputValidation.java): Responsible for valid user input.
- [Commands.java](src/main/java/edu/ntnu/stud/commands): Responsible for the applications functionalities, divided in nine classes each responsible for its own functionality.

[Test](src/test): Contains the JUnit-test classes for the application
- [RegisterTest](src/test/java/edu/ntnu/stud/RegisterTest.java): Includes tests for the Register class
- [TrainDepartureTest](src/test/java/edu/ntnu/stud/TrainDepartureTest.java): Includes tests for the TrainDeparture class
- [CommandTest](src/test/java/edu/ntnu/stud/CommandTest.java): Contains tests for the Commands class
## Link to repository
Link to repository: 

https://gitlab.stud.idi.ntnu.no/johanfix/train-dispatch-system/-/tree/master

[//]: # (TODO: Include a link to your repository here.)

## How to run the project
Before running the application make sure that: 
- Java Devopment Kit (JDK) 17 is installed
- Apache Maven is installed
- you use an IDE which support Java, recommend using IntelliJ

Running the application:
- download the zip file for the project
- unzip the project folder [TrainDispatchSystem](../IDATT1003-2023-Mappe-TrainDispatchSystem-Template) and open the folder in your IDE
- start the application by opening the [TrainDispatchApp.java](src/main/java/edu/ntnu/stud/TrainDispatchApp.java) and push the start button.


## How to run the tests
This project uses Maven to manage the running of tests. To run the tests for the project, you''ll need
to have Maven installed and configured on your system.

To execute tests
1. Open the [test folder](src/test) 
2. Select the test file that shares the same name as the class you intend to test
3. Push the (green) button next to the testes



## References

